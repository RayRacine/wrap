#lang typed/racket/base

(provide
 log-swf-error
 log-swf-info
 log-swf-debug)

(define-logger swf)
